<?php
declare(strict_types=1);

namespace App\Domain\Animal;

interface AnimalRepository {
    
    public function findAll(): array;
    
    public function findVivos(): array;
    
    public function getId(int $id): array;

    public function salvar($editar, $id, $nome, $especie, $raca, $sexo, $nascimento, $falecimento, $atualizacao);

}
