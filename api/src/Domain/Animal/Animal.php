<?php
declare(strict_types=1);

namespace App\Domain\Animal;

use JsonSerializable;

class Animal implements JsonSerializable {
    
    private $id;
    
    private $idraca;

    private $nome;

    private $sexo;

    private $especie;
    
    private $nascimento;
    
    private $atualizacao;
    
    private $falecimento;

    public function __construct(?int $id, ?int $idraca, string $nome, string $sexo, int $especie, string $nascimento, string $atualizacao, string $falecimento)
    {
        $this->id = $id;
        $this->idraca = $idraca;
        $this->nome = $nome;
        $this->sexo = $sexo;
        $this->especie = $especie;
        $this->nascimento = $nascimento;
        $this->atualizacao = $atualizacao;
        $this->falecimento = $falecimento;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getIdRaca(): ?int
    {
        return $this->idraca;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function getSexo(): string
    {
        return $this->sexo;
    }

    public function getEspecie(): int
    {
        return $this->especie;
    }
    
    public function getNascimento(): string
    {
        return $this->nascimento;
    }
    
    public function getAtualizacao(): string
    {
        return $this->atualizacao;
    }
    
    public function getFalecimento(): string
    {
        return $this->falecimento;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'idraca' => $this->idraca,
            'nome' => $this->nome,
            'sexo' => $this->sexo,
            'especie' => $this->especie,
            'nascimento' => $this->nascimento,
            'atualizacao' => $this->atualizacao,
            'falecimento' => $this->falecimento,
        ];
    }
}
