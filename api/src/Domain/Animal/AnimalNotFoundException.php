<?php
declare(strict_types=1);

namespace App\Domain\Animal;

use App\Domain\DomainException\DomainRecordNotFoundException;

class AnimalNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'O animal que requisitou não existe.';
}
