<?php
declare(strict_types=1);

namespace App\Domain\Raca;

use JsonSerializable;

class Raca implements JsonSerializable {
    
    private $id;

    private $nome;

    private $situacao;

    private $especie;

    public function __construct(?int $id, string $nome, string $situacao, string $especie)
    {
        $this->id = $id;
        $this->nome = $nome;
        $this->situacao = $situacao;
        $this->especie = $especie;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function getSituacao(): string
    {
        return $this->situacao;
    }

    public function getEspecie(): string
    {
        return $this->especie;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'situacao' => $this->situacao,
            'especie' => $this->especie,
        ];
    }
}
