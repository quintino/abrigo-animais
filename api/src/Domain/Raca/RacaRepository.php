<?php
declare(strict_types=1);

namespace App\Domain\Raca;

interface RacaRepository {
    
    public function findRacas(): array;

}
