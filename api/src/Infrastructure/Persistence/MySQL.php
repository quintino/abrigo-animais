<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

class MySQL {
    
    public function connect() {
        $server = 'localhost';
        $user = 'root';
        $pass = 'password';
        $database = 'abrigo';
        $connection = new \mysqli($server, $user, $pass, $database);
        return $connection;
    }
    
}
