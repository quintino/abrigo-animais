<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Animal;

use App\Domain\Animal\AnimalRepository;
use App\Domain\Animal\Animal;
use App\Infrastructure\Persistence\MySQL;
use App\Domain\Animal\AnimalNotFoundException;

class MyAnimalRepository extends MySQL implements AnimalRepository {
    
    private $animais;

    public function __construct(array $animais = null) {
        $this->animais = $animais;
    }

    public function findAll(): array {
        $db = $this->connect();
        $result = $db->query('SELECT animal.*, raca.nome AS nome_raca FROM animal INNER JOIN raca ON raca.id = animal.id_raca;');
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    
    public function findVivos(): array {
        $db = $this->connect();
        $result = $db->query('SELECT animal.*, raca.nome AS nome_raca FROM animal INNER JOIN raca ON raca.id = animal.id_raca WHERE animal.data_falecimento IS NULL;');
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function getId(int $id): array {
        $db = $this->connect();
        $result = $db->query('SELECT animal.*, raca.nome AS nome_raca FROM animal INNER JOIN raca ON raca.id = animal.id_raca WHERE animal.id = '.$id.';');
        $obj = $result->fetch_array(MYSQLI_ASSOC);
        if (is_null($obj)) {
            throw new AnimalNotFoundException();
        }
        return $obj;
    }

    public function salvar($editar, $id, $nome, $especie, $raca, $sexo, $nascimento, $falecimento, $atualizacao) {
        $db = $this->connect();
        if ($editar == 1) {
            $db->query('UPDATE animal SET nome = "'.$nome.'", data_nascimento = "'.$nascimento.'", data_atualizacao = "'.$atualizacao.'", data_falecimento = "'.$falecimento.'" WHERE id = '.$id);
        } else {
            $db->query('INSERT INTO animal(id_raca, nome, sexo, especie, data_nascimento, data_atualizacao, data_falecimento) 
                    VALUES('.$raca.', "'.$nome.'", "'.$sexo.'", '.$especie.', "'.$nascimento.'", "'.$atualizacao.'", "'.$falecimento.'")');
        }
    }
    
}
