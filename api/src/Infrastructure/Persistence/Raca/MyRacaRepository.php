<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Raca;

use App\Infrastructure\Persistence\MySQL;
use App\Domain\Raca\RacaRepository;

class MyRacaRepository extends MySQL implements RacaRepository {
    
    private $racas;

    public function __construct(array $racas = null) {
        $this->racas = $racas;
    }
    
    public function findRacas(): array {
        $db = $this->connect();
        $result = $db->query('SELECT * FROM raca WHERE situacao = 1;');
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

}
