<?php
declare(strict_types=1);

namespace App\Application\Actions\Raca;

use Psr\Http\Message\ResponseInterface as Response;

class ListRacasAction extends RacaAction {
    
    protected function action(): Response {
        $racas = $this->racaRepository->findRacas();
        $this->logger->info("Lista das raças foi visualizada.");
        return $this->respondWithData($racas);
    }

}
