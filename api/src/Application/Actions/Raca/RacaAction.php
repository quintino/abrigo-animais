<?php
declare(strict_types=1);

namespace App\Application\Actions\Raca;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use App\Domain\Raca\RacaRepository;

abstract class RacaAction extends Action {
    protected $racaRepository;

    public function __construct(LoggerInterface $logger, RacaRepository $racaRepository) {
        parent::__construct($logger);
        $this->racaRepository = $racaRepository;
    }
    
}
