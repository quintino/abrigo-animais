<?php
declare(strict_types=1);

namespace App\Application\Actions\Animal;

use Psr\Http\Message\ResponseInterface as Response;

class SalvarAnimalAction extends AnimalAction {

    protected function action(): Response {
        $id = $this->request->getParsedBody()["id"];
        $editar = $this->request->getParsedBody()["editar"];
        $nome = $this->request->getParsedBody()["nome"];
        $especie = $this->request->getParsedBody()["especie"];
        $raca = $this->request->getParsedBody()["raca"];
        $sexo = $this->request->getParsedBody()["sexo"];
        $nascimento = $this->request->getParsedBody()["nascimento"];
        $falecimento = $this->request->getParsedBody()["falecimento"];
        $atualizacao = $this->request->getParsedBody()["atualizacao"];
        $this->animalRepository->salvar($editar, $id, $nome, $especie, $raca, $sexo, $nascimento, $falecimento, $atualizacao);
        $this->logger->info("Animal registrado.");
        return $this->respondWithData("");
    }

}
