<?php
declare(strict_types=1);

namespace App\Application\Actions\Animal;

use Psr\Http\Message\ResponseInterface as Response;

class ListAnimaisAction extends AnimalAction {
    
    protected function action(): Response {
        $animais = $this->animalRepository->findAll();
        $this->logger->info("Lista dos animais foi visualizada.");
        return $this->respondWithData($animais);
    }

}
