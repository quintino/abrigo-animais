<?php
declare(strict_types=1);

namespace App\Application\Actions\Animal;

use App\Application\Actions\Action;
use App\Domain\Animal\AnimalRepository;
use Psr\Log\LoggerInterface;

abstract class AnimalAction extends Action {
    protected $animalRepository;

    public function __construct(LoggerInterface $logger, AnimalRepository $animalRepository) {
        parent::__construct($logger);
        $this->animalRepository = $animalRepository;
    }
    
}
