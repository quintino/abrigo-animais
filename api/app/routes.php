<?php
declare(strict_types=1);

use App\Application\Actions\Animal\ListAnimaisAction;
use App\Application\Actions\Animal\ListAnimaisDoarAction;
use App\Application\Actions\Animal\SalvarAnimalAction;
use App\Application\Actions\Animal\ViewAnimalAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\Actions\Raca\ListRacasAction;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/animais', function (Group $group) {
        $group->get('', ListAnimaisAction::class);
        $group->get('/{id}', ViewAnimalAction::class);
    });
        
    $app->get('/racas', ListRacasAction::class);
    $app->get('/doar', ListAnimaisDoarAction::class);
    $app->get('/salvar', SalvarAnimalAction::class);
    $app->post('/salvar', SalvarAnimalAction::class);
};
