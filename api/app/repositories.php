<?php
declare(strict_types=1);

use App\Domain\Animal\AnimalRepository;
use App\Infrastructure\Persistence\Animal\MyAnimalRepository;
use DI\ContainerBuilder;
use App\Domain\Raca\RacaRepository;
use App\Infrastructure\Persistence\Raca\MyRacaRepository;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        AnimalRepository::class => \DI\autowire(MyAnimalRepository::class),
        RacaRepository::class => \DI\autowire(MyRacaRepository::class),
    ]);
};
