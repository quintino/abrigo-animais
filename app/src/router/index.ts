import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/doacao',
    name: 'doacao',
    component: () => import(/* webpackChunkName: "about" */ '../views/Doacao.vue')
  },
  {
    path: '/racas',
    name: 'racas',
    component: () => import(/* webpackChunkName: "about" */ '../views/Racas.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
