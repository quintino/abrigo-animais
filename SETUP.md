O projeto ficou bem simples, pois devido ao curto tempo, ele ficou com um layout bem simples e crú.

Dentro da pasta DB tem o dump do banco de dados já com algumas raças incluídas.

Se forem rodar o projeto dentro de uma máquina linux, logo após o pull no GIT, iremos até a pasta app do projeto e iremos digitar: yarn install

Com esse comando iremos efetuar o download dos pacotes necessários para o projeto web do Vue.JS.

Dentro da pasta API, depois de terem já o composer devidamente instalado na máquina executem o comando: composer install

Com esse comando iremos efetuar o download dos pacotes necessários para o projeto api em Slim Framework.

Feito isso, iremos subir e depois de subir o dump no mysql, iremos na pasta API e executaremos o comando: ./start.sh

Ao executar a api na porta 8080 iremos para a pasta app e executaremos: yarn serve

Subindo assim a app na porta 8000 e para acessar a app basta ir em: http://localhost:8000